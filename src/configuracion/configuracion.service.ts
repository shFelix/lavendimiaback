import { Injectable } from '@nestjs/common';
import { Configuracion } from './interfaces/configuracion.interface';
import { GuardarConfiguracionDto } from './dto/crear-configuracion.dto';

@Injectable()
export class ConfiguracionService {
  private readonly configuracion : Configuracion = {
    tasaFinanciamiento: 2.8,
    plazoMaximo: 12,
    porcentajeEnganche: 20
  };

  getConfiguracion() : Configuracion{
    return this.configuracion;
  }

  guardarConfiguracion(configuracion : GuardarConfiguracionDto) : boolean {

    //Actualiza la configuración
    this.configuracion.plazoMaximo = configuracion.plazoMaximo;
    this.configuracion.porcentajeEnganche = configuracion.porcentajeEnganche;
    this.configuracion.tasaFinanciamiento = configuracion.tasaFinanciamiento;

    return true;
  }

}
