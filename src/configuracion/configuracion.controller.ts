import { Controller, Get, Post, Body } from '@nestjs/common';
import { ConfiguracionService } from './configuracion.service';
import { GuardarConfiguracionDto } from './dto/crear-configuracion.dto';

@Controller('configuracion')
export class ConfiguracionController {

  constructor(private readonly _servicio : ConfiguracionService){}

  @Get()
  getConfiguracion(){
    return this._servicio.getConfiguracion();
  }

  @Post()
  guardarConfiguracion(@Body() guardarconfiguracion : GuardarConfiguracionDto){
    return this._servicio.guardarConfiguracion(guardarconfiguracion);
  }
}
