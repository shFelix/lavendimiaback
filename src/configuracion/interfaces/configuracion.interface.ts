export interface Configuracion{
  tasaFinanciamiento : number;
  porcentajeEnganche : number;
  plazoMaximo : number;
}