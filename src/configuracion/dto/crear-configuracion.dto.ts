export class  GuardarConfiguracionDto{
  readonly tasaFinanciamiento : number;
  readonly porcentajeEnganche : number;
  readonly plazoMaximo : number;
}