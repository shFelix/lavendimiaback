import { Injectable } from '@nestjs/common';
import { Configuracion } from '../configuracion/interfaces/configuracion.interface';
import { GenerarAbonosDto } from './dto/generar-abonos.dto';
import { Abono } from './interfaces/abono.interface';

@Injectable()
export class AbonosService {

  getPlazosDisponibles(generarAbonos : GenerarAbonosDto, configuracion : Configuracion){
    let precioContado : number;
    let abonosDisponibles : Abono[] = new Array();

    precioContado = generarAbonos.totalAdeudo / (1 + ((configuracion.tasaFinanciamiento * configuracion.plazoMaximo) / 100));

    for (let plazo = 3; plazo <= configuracion.plazoMaximo; plazo += 3){
      let abono : Abono = {
        plazo: plazo,
        totalPagar: 0,
        importeAbono: 0,
        ahorro: 0
      };

      abono.totalPagar = precioContado * (1 + (configuracion.tasaFinanciamiento * plazo) / 100);
      abono.importeAbono = abono.totalPagar / plazo;
      abono.ahorro = generarAbonos.totalAdeudo - abono.totalPagar;

      abonosDisponibles.push(abono);
    }

    return abonosDisponibles;

  }
}
