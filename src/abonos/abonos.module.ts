import { Module } from '@nestjs/common';
import { AbonosController } from './abonos.controller';
import { AbonosService } from './abonos.service';
import { ConfiguracionService } from '../configuracion/configuracion.service';

@Module({
  controllers: [AbonosController],
  providers: [
    AbonosService,
    ConfiguracionService]
})
export class AbonosModule {}
