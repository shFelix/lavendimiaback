export interface Abono{
  plazo : number;
  totalPagar : number;
  importeAbono : number;
  ahorro : number;
}