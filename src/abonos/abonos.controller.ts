import { Controller, Get, Post, Body } from '@nestjs/common';
import { AbonosService } from './abonos.service';
import { ConfiguracionService } from '../configuracion/configuracion.service';
import { GenerarAbonosDto } from './dto/generar-abonos.dto';
import { Abono } from './interfaces/abono.interface';

@Controller('abonos')
export class AbonosController {

  constructor(private readonly _servicioAbonos : AbonosService, 
              private readonly _servicioConfiguracion : ConfiguracionService){}


  @Post()
  getPlazosDisponbiles(@Body() generarAbonos : GenerarAbonosDto) : Abono[] {
    return this._servicioAbonos.getPlazosDisponibles(generarAbonos, 
                                                     this._servicioConfiguracion.getConfiguracion());
  }
}
