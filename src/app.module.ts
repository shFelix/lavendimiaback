import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClientesModule } from './clientes/clientes.module';
import { ArticulosModule } from './articulos/articulos.module';
import { ConfiguracionModule } from './configuracion/configuracion.module';
import { VentasModule } from './ventas/ventas.module';
import { AbonosModule } from './abonos/abonos.module';

@Module({
  imports: [ClientesModule, ArticulosModule, ConfiguracionModule, VentasModule, AbonosModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
