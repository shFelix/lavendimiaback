import { Controller, Get, Post, Body } from '@nestjs/common';
import { ArticulosService } from './articulos.service';
import { CrearArticuloDto } from './dto/crear-articulo.dto';

@Controller('articulos')
export class ArticulosController {

  constructor(private readonly _servicio : ArticulosService){}

  @Get()
  getArticulos(){
    return this._servicio.getArticulos();
  }

  @Post()
  crearArticulo(@Body() crearArticulo : CrearArticuloDto ){
    return this._servicio.crearArticulo(crearArticulo);
  }
}
