import { Injectable } from '@nestjs/common';
import { CrearArticuloDto } from './dto/crear-articulo.dto';
import { Articulo } from './interfaces/articulo.interface';

@Injectable()
export class ArticulosService {
  private readonly articulos : Articulo[] = [
    {
      id: 1,
      descripcion: "Recámara King Size",
      modelo: "MOD-REC-KZ",
      precio: 7500,
      existencia: 4
    },
    {
      id: 2,
      descripcion: "Comedor 4 sillas",
      modelo: "MOD-COM-04",
      precio: 3500,
      existencia: 2
    },
    {
      id: 3,
      descripcion: "Escritorio",
      modelo: "MOD-ESC-01",
      precio: 2000,
      existencia: 0
    }
  ];

  getArticulos() : Articulo[]{
    return this.articulos;
  }

  crearArticulo(articulo : CrearArticuloDto) : number {
    return this.articulos.push(articulo);
  }
  
}
