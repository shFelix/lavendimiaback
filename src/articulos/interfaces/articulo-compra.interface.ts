import { Articulo } from "./articulo.interface";

export interface ArticuloCompra{
  precio : number;
  cantidad : number;
  articulo : Articulo;
}