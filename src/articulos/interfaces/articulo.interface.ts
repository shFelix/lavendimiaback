export interface Articulo{
  id : number;
  descripcion : string;
  modelo : string;
  precio : number;
  existencia : number;
  
}