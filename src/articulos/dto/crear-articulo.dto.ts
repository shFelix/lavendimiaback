export class CrearArticuloDto{
  readonly id: number;
  readonly descripcion : string;
  readonly modelo : string;
  readonly precio : number;
  readonly existencia : number;
  
}