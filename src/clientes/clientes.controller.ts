import { Controller, Get, Req, Post, Body } from '@nestjs/common';
import { ClientesService } from './clientes.service';
import { Cliente } from './interfaces/cliente.interface';
import { CrearClienteDto } from './dto/crear-cliente.dto';

@Controller('clientes')
export class ClientesController {

  constructor(private readonly _servicio : ClientesService){
    
  }

  @Get()
  getClientes(){
    return this._servicio.getClientes();
  }

  @Post()
  crearCliente(@Body() crearCliente : CrearClienteDto) {
    return this._servicio.crearCliente(crearCliente);
  }

}
