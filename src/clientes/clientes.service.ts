import { Injectable } from '@nestjs/common';
import { CrearClienteDto } from './dto/crear-cliente.dto';
import { Cliente } from './interfaces/cliente.interface';

@Injectable()
export class ClientesService {
  private readonly clientes: Cliente[] = [
    {
      id: 1,
      nombre: "Sheryl",
      apellidoPaterno: "Félix",
      apellidoMaterno: "Rodríguez",
      rfc: "FERS910828123"
    },
    {
      id: 2,
      nombre: "Jorge",
      apellidoPaterno: "Romero",
      apellidoMaterno: "Polo",
      rfc: "ROPJ940211321"
    },
    {
      id: 3,
      nombre: "Violeta",
      apellidoPaterno: "Leyva",
      apellidoMaterno: "Félix",
      rfc: "LEFA001107213"
    }
  ];


  getClientes() : Cliente[]{
    return this.clientes;
  }

  crearCliente(cliente : CrearClienteDto): number {
    let nuevoCliente : Cliente;

    nuevoCliente.id = this.clientes.length + 1;
    nuevoCliente.nombre = cliente.nombre;
    nuevoCliente.apellidoPaterno = cliente.apellidoPaterno;
    nuevoCliente.apellidoMaterno = cliente.apellidoMaterno;
    nuevoCliente.rfc = cliente.rfc;
    
    return this.clientes.push(nuevoCliente);
  }
}
