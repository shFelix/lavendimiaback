export class CrearClienteDto {
  readonly nombre : string;
  readonly apellidoPaterno : string;
  readonly apellidoMaterno : string;
  readonly rfc : string;

}