export interface Cliente {
  id : number;
  nombre : string;
  apellidoPaterno : string;
  apellidoMaterno : string;
  rfc : string;
}