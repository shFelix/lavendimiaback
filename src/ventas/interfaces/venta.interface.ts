import { Cliente } from "src/clientes/interfaces/cliente.interface";
import { Abono } from "src/abonos/interfaces/abono.interface";
import { ArticuloCompra } from "src/articulos/interfaces/articulo-compra.interface";

export interface Venta {
  id : number;
  cliente : Cliente;
  listaArticulos : ArticuloCompra[];
  fechaVenta : Date;
  plazoSeleccionado : Abono;
  tasaInteres : number;
  enganche : number;
}