import { Cliente } from "src/clientes/interfaces/cliente.interface";
import { Articulo } from "src/articulos/interfaces/articulo.interface";
import { Abono } from "src/abonos/interfaces/abono.interface";

export class CrearVentaDto{
  readonly idCliente : number;
  readonly listaArticulos : Articulo[];
  readonly plazoSeleccionado : Abono;
  readonly tasaInteres : number;
  readonly enganche : number;
}