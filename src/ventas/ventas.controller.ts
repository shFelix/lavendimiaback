import { Controller, Post, Body } from '@nestjs/common';
import { VentasService } from './ventas.service';
import { CrearVentaDto } from './dto/crear-venta.dto';

@Controller('ventas')
export class VentasController {

  constructor(private readonly _servicioVentas : VentasService){}


  @Post()
  crearVenta(@Body() crearVenta : CrearVentaDto){
    return this._servicioVentas.crearVenta(crearVenta);
  }


}
